const path = require("path");
const fs = require("fs");
const { request } = require("../utils/request");
const handlebars = require("handlebars");
const chalk = require('chalk');
const { mkdirsSync } = require("../utils");

const { apifox = {} } = require(path.join(process.cwd(), 'apifox.config.js'));

let dataSchemas;

let tplArgs = {
    requestMethodPath: apifox.requestMethodPath,
}

const dataTypeMap = {
    integer: 'number',
    long: 'string'
}

String.prototype.firstLetterUpperCase = function () {
    return this.charAt(0).toUpperCase() + this.slice(1);
}

exports.getApiList = function () {
    return request({
        url: '/api-details',
    })
}

exports.getProjectDataSchema = async function getProjectDataSchema() {
    const data = await request({
        url: `/projects/${apifox.projectId}/data-schemas`
    });
    dataSchemas = data;
}

function generateTsField(params) {
    return params?.map(p => ({
        name: p.name,
        type: p.type === 'array' ? '(string|number)[]' : p.type,
        description: p.description,
        required: p.required
    }))
}

function generateObjectTsFields(obj = {}, excludeFields = []) {
    if (obj.$ref) {
        const schemaId = /\d+/.exec(obj.$ref)?.[0];
        const dataSchema = (dataSchemas || []).find(s => s.id === Number(schemaId));
        if (dataSchema?.jsonSchema) {
            return generatePropertiesTsField(dataSchema.jsonSchema, excludeFields);
        }
    }

    let fields = generatePropertiesTsField(obj, excludeFields);
    if (obj["x-apifox-refs"]) {
        fields = fields.concat(generateApifoxRefsFields(obj["x-apifox-refs"]));
    }
    return fields;
}

function getArrayTsType(items, field) {
    if (items.$ref) {
        const schemaId = /\d+/.exec(items.$ref)?.[0];
        const dataSchema = (dataSchemas || []).find(s => s.id === Number(schemaId));
        if (dataSchema?.name) {
            const parentType = tplArgs.interfaces[tplArgs.interfaces.length - 1];
            const isRecursiveRef = parentType && items.$ref === parentType.originalRef;

            if (!isRecursiveRef) {
                if (!processedSchemaIds.has(schemaId)) {
                    processedSchemaIds.add(schemaId);
                    if (dataSchema.jsonSchema) {
                        tplArgs.interfaces.push({
                            name: dataSchema.name,
                            fields: generateObjectTsFields(dataSchema.jsonSchema),
                            originalRef: items.$ref
                        });
                    }
                }
            }
            return `${dataSchema.name}[]`;
        }
    }

    let type = (dataTypeMap[items.type] || items.type) + '[]';
    if (items.type === 'object' || items.$ref) {
        const typeName = field.firstLetterUpperCase() + 'ArrayItemType';
        
        const parentType = tplArgs.interfaces[tplArgs.interfaces.length - 1];
        const isRecursiveRef = items.$ref && parentType && 
            items.$ref === parentType.originalRef;
        
        if (isRecursiveRef) {
            return `${parentType.name}[]`;
        }
        
        tplArgs.interfaces.push({
            name: typeName,
            fields: generateObjectTsFields(items),
            originalRef: items.$ref
        })
        type = typeName + '[]';
    }
    return type;
}

function generatePropertiesTsField(obj = {}, excludeFields = []) {
    const fields = [];
    Object.keys(obj.properties || {}).forEach(field => {
        if (excludeFields.includes(field)) return;
        const fieldObj = obj.properties[field];
        let type = fieldObj.type;
        if (fieldObj.type === 'array') {
            type = getArrayTsType(fieldObj.items, field);
        } else if (fieldObj.type === 'object' || fieldObj.$ref) {
            type = field.firstLetterUpperCase() + 'Type';
            tplArgs.interfaces.push({
                name: type,
                fields: generateObjectTsFields(fieldObj, [])
            })
        }
        fields.push({
            name: field,
            type: dataTypeMap[type] || type,
            description: fieldObj.description,
            required: (obj.required || []).includes(field)
        })
    });
    return fields;
}

let processedSchemaIds = new Set();

function generateApifoxRefsFields(refs) {
    let fields = [];
    if (refs) {
        Object.keys(refs).forEach(key => {
            const ref = refs[key];
            const schemaId = /\d+/.exec(ref.$ref)?.[0];
            
            const dataSchema = (dataSchemas || []).find(s => s.id === Number(schemaId));
            if (dataSchema?.jsonSchema) {
                if (!processedSchemaIds.has(schemaId)) {
                    processedSchemaIds.add(schemaId);
                    tplArgs.interfaces.push({
                        name: dataSchema.name,
                        fields: generateObjectTsFields(dataSchema.jsonSchema),
                        originalRef: ref.$ref
                    });
                }
                
                const refFields = generatePropertiesTsField(
                    dataSchema.jsonSchema,
                    Object.keys(ref["x-apifox-overrides"] || {})
                );
                fields = fields.concat(refFields);
            }
            
            const overrideFields = generateApifoxOverrideFields(
                ref["x-apifox-overrides"],
                ref.required || []
            );
            fields = fields.concat(overrideFields);
        });
    }
    return fields;
}

function generateApifoxOverrideFields(overrides, required = []) {
    let fields = [];
    if (overrides) {
        Object.keys(overrides).forEach(field => {
            const obj = overrides[field];
            let type = obj.type;
            if (obj && (obj.type === 'object' || obj.$ref)) {
                type = field.firstLetterUpperCase() + 'Type';
                tplArgs.interfaces.push({
                    name: type,
                    fields: generateObjectTsFields(obj),
                })
            } else if (obj && obj.type === 'array') {
                type = getArrayTsType(obj.items, field);
            }
            fields.push({
                name: field,
                type: dataTypeMap[type] || type,
                description: obj.description,
                required: required.includes(field),
            });
        })
    }
    return fields;
}

function generateResponseTsField(responses) {
    (responses || []).forEach(response => {
        if (response.code === 200) {
            const jsonSchema = response.jsonSchema;
            const schema = jsonSchema.allOf ? jsonSchema.allOf[0] : jsonSchema;
            
            // 直接使用现有的函数处理所有字段
            const fields = generateObjectTsFields(schema);
            if (fields.length > 0) {
                tplArgs.interfaces.push({
                    name: 'ResponseBody',
                    fields
                });
            }
        }
    });
}

function generateFileName(api) {
    const pathWithoutVersion = api.path.replace(/^\/v\d+/, '');
    
    const pathParts = pathWithoutVersion.split('/')
        .filter(Boolean)
        .map(item => item
            .replace(/^\{(.+)\}$/, 'By$1')
            .replace(/[-_]([a-z])/g, (_, letter) => letter.toUpperCase())
            .firstLetterUpperCase()
        );

    const fileName = [
        api.method.toLowerCase(),
        ...pathParts
    ].join('') + '.ts';

    return fileName;
}

exports.generateTsTypeFile = function (api, fileName) {
    console.log(chalk.green(`***********正在生成 ${api.name} 接口***********`));
    processedSchemaIds = new Set();
    tplArgs = {
        ...tplArgs,
        apiId: api.id,
        name: api.name,
        path: api.path,
        method: api.method,
        interfaces: []
    }
    const query = api.parameters.query;
    tplArgs.interfaces.push({
        name: 'RequestQuery',
        fields: generateTsField(query)
    })
    const requestBody = api.requestBody;
    const parameters = requestBody && requestBody.parameters;
    tplArgs.interfaces.push({
        name: 'RequestBody',
        fields: generateTsField(parameters).concat(generateObjectTsFields(requestBody.jsonSchema))
    })
    const responses = api.responses;
    generateResponseTsField(responses);
    const tpl = fs.readFileSync(path.join(__dirname, '../tpl/request.tpl'), 'utf-8');
    const content = handlebars.compile(tpl)(tplArgs)
    const targetPath = path.join(process.cwd(), apifox.output);
    if (!fs.existsSync(targetPath)) {
        mkdirsSync(targetPath);
    }
    fs.writeFileSync(path.join(targetPath, fileName || generateFileName(api)), content);
}

function generateModuleIndexFile() {
    console.log(chalk.greenBright('正在生成导出文件, 请稍候......'));
    // 生成导出文件
    const rootDir = path.join(process.cwd(), apifox.output);
    if (!fs.existsSync(rootDir)) {
        mkdirsSync(rootDir);
    }
    const templateContent = fs.readFileSync(path.resolve(__dirname, '../tpl/index.tpl')).toString();
    const files = fs.readdirSync(rootDir).filter(item => !['index.ts', 'api.lock.json'].includes(item)).map(item => item.replace('.ts', ''));
    const content = handlebars.compile(templateContent)({ files });
    fs.writeFileSync(path.join(rootDir, 'index.ts'), content);
    console.log(chalk.greenBright('生成导出文件成功'));
}

exports.generateModuleIndexFile = generateModuleIndexFile;

exports.generateLockFile = function () {
    // 生成lock文件(根据制定目录经存在的文件生成)
    console.log(chalk.greenBright('正在生成lock文件, 请稍候......'));
    const rootDir = path.join(process.cwd(), apifox.output);
    const files = {};
    const modules = fs.readdirSync(rootDir).filter(item => !['index.ts', 'api.lock.json'].includes(item)).map(item => item.replace('.ts', ''));
    modules.forEach(file => {
        const filePath = path.join(rootDir, `${file}.ts`);
        const fileContent = fs.readFileSync(filePath).toString();
        const apiId = fileContent.match(/export const apiId = '(.*?)'/)?.[1];
        if (apiId) {
            files[apiId] = file;
        }
    });
    fs.writeFileSync(path.join(rootDir, 'api.lock.json'), JSON.stringify(files, null, 2));
    console.log(chalk.greenBright('生成lock文件成功'));

    generateModuleIndexFile();
}
