const path = require('path');
const axios = require("axios");

const { apifox } = require(path.join(process.cwd(), 'apifox.config.js'));

let token = "";

axios.defaults.baseURL = 'https://api.apifox.com/api/v1/';

axios.interceptors.request.use((config => {
    config.method = config.method || 'GET';
    if (!config.params) {
        config.params = {};
    }
    config.params.locale = 'zh-CN';
    if (!config.headers) {
        config.headers = {};
    }
    if (token) {
        config.headers.Authorization = token;
        config.headers["X-Project-Id"] = apifox.projectId;
    }
    return config;
}))

axios.interceptors.response.use(res => {
    const data = res.data;
    if (data.success) {
        return data.data;
    }
    return res.data;
}, err => {
    throw err.response.data.errorMessage;
})

module.exports = {
    request(options) {
        return axios(options);
    },
    setToken(t) {
        token = t;
    }
}