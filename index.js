#!/usr/bin/env node
const path = require("path");
const fs = require("fs");
const { program } = require("commander");
const login = require("./services/login");
const { getApiList, generateTsTypeFile, getProjectDataSchema, generateLockFile } = require("./services/api");

let apifox = null;

const configPath = path.join(process.cwd(), 'apifox.config.js');
if (fs.existsSync(configPath)) {
    apifox = require(configPath).apifox;
}

if (!apifox) {
    throw new Error('请先在跟目录创建apifox.config.json配置apifox相关信息');
}

async function create(ids) {
    // 登录
    await login();
    // 获取通用数据模型
    await getProjectDataSchema();
    // 获取接口列表
    let list = await getApiList();
    const lockFilePath = path.join(process.cwd(), apifox.output, 'api.lock.json');
    let apis = {};
    if (fs.existsSync(lockFilePath)) {
        apis = require(lockFilePath);
    }
    if (ids.length > 0) {
        list = list.filter(item => ids.includes(item.id + ''));
    }
    // 依次生成文件
    list.forEach((api) => {
        generateTsTypeFile(api, apis[api.id] ? apis[api.id] + '.ts' : undefined);
    });

    generateLockFile();
}
program.command('create')
    .description('根据apifox接口生成ts类型文件')
    .option('--id <id>', '根据id生成')
    .action(({ id }) => {
        create(id ? id.split(",") : []);
    });

program.command("lock")
    .action(generateLockFile);

program.parse(process.argv);