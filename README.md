# @fixedx/apifox-to-typescript

将 ApiFox 的接口转换为 TypeScript 接口

## 使用方法

### 1. 安装依赖

```bash
# 使用npm
npm install @fixedx/apifox-to-typescript

# 使用yarn
yarn add @fixedx/apifox-to-typescript

# 使用pnpm
pnpm install @fixedx/apifox-to-typescript
```

### 2. 创建配置文件

在项目根目录下的 apifox.config.js 配置以下内容：

```javascript
"apifox": {
    "auth": {
      "account": "youraccount",
      "password": "yourpassword"
    },
    "projectId": "yourproject",
    "output": "src/apis", // 接口输出目录
    "requestMethodPath": "@/utils/request" // 请求方法目录, 需自行提供
}
```

### 3. 命令介绍

- att create 命令

根据配置文件中的信息，从 ApiFox 获取接口信息，并生成对应的 TypeScript 接口文件。

```bash
npx att create
```

- att lock 命令

重新生成 api.lock.json 文件，用于锁定接口文件名。

```bash
npx att lock
```
